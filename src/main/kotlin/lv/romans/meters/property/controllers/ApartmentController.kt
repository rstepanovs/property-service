package lv.romans.meters.property.controllers

import lv.romans.meters.commons.api.ApartmentForm
import lv.romans.meters.property.service.ApartmentService
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("apartments")
class ApartmentController(private val apartmentService: ApartmentService) {


    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping
    fun addApartment(@RequestParam("propertyId") propertyId: Long, @RequestBody form: ApartmentForm): Long {
        return apartmentService.createNewApartment(form, propertyId)
    }

}