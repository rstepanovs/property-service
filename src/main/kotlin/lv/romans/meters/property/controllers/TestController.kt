package lv.romans.meters.property.controllers

import lv.romans.meters.property.service.PropertiesService
import lv.romans.meters.property.service.PropertyRepository
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Profile("test")
@RestController
@RequestMapping("test")
class TestController(private val repo: PropertyRepository) {

    @GetMapping("resetProperty/{id}")
    fun reset(@PathVariable id: Long) {
        val previous = repo.findById(id).get()
        repo.save(previous.copy(submitTo = 0, submitFrom = 0,
                billingTo = 0, billingFrom = 0))
    }


}