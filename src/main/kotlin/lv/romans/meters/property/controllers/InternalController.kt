package lv.romans.meters.property.controllers

import lv.romans.meters.commons.services.CanUserDownloadBill
import lv.romans.meters.commons.services.PropertyList
import lv.romans.meters.property.service.ApartmentService
import lv.romans.meters.property.service.PropertiesService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("internal")
class InternalController(private val service: PropertiesService, private val apartmentService: ApartmentService) {

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("list")
    fun getAllProperties(): PropertyList = service.getAllProperties()

    @PostMapping("verifyBill")
    fun canUploadBill(info: CanUserDownloadBill) =
            apartmentService.verifyBillOwner(info)
}