package lv.romans.meters.property.controllers

import lv.romans.meters.commons.api.ManagerDashboard
import lv.romans.meters.commons.api.NewProperty
import lv.romans.meters.commons.api.PropertyForm
import lv.romans.meters.commons.services.PropertyList
import lv.romans.meters.property.service.PropertiesService
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.logging.Level
import java.util.logging.Logger

@RestController
@RequestMapping("properties")
class PropertiesController(private val service: PropertiesService) {

    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("add")
    fun addProperty(@RequestBody property: NewProperty): Long {
        return service.addProperty(property)
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping
    fun deleteProperty(@RequestParam("propertyId") id: Long) {
        service.delete(id)
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping
    fun changeManager(@RequestParam("id") id: Long,
                      @RequestParam("manager") managerEmail: String) {
        service.changeManager(id, managerEmail)
    }

    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("my")
    fun managerProperties(principal: Principal): ManagerDashboard {
        val properties = service.gePropertiesFor(principal.name)
        Logger.getAnonymousLogger().log(Level.SEVERE, properties.toString())
        Logger.getAnonymousLogger().log(Level.SEVERE, principal.name)
        return ManagerDashboard(properties)
    }

    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("complete/{id}")
    fun finishPropertySetup(@PathVariable("id") id: Long,
                            @RequestBody propertyForm: PropertyForm) {
        service.setupProperty(id, propertyForm)
    }

    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("setup")
    fun propertySetupState(@RequestParam("id") id: Long) = service.setupState(id)
}