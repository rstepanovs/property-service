package lv.romans.meters.property.messaging

import lv.romans.meters.commons.messaging.BillUploaded
import lv.romans.meters.property.service.ApartmentService
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PostMapping

@Component
class MessageListener(private val apartmentService: ApartmentService) {

    @RabbitListener(queues = ["bill_uploaded"])
    fun billUploaded(bill: BillUploaded) {
        apartmentService.saveBill(bill)
    }
}
