package lv.romans.meters.property.service

import lv.romans.meters.commons.UserRole
import lv.romans.meters.commons.api.ApartmentForm
import lv.romans.meters.commons.messaging.BillUploaded
import lv.romans.meters.commons.messaging.NewUser
import lv.romans.meters.commons.services.CanUserDownloadBill
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.persistence.*

@Service
class ApartmentService(
        private val repository: ApartmentRepository,
        private val billRepository: BillRepository,
        private val verifyPropertyOwner: VerifyPropertyOwner,
        private val propertiesService: PropertiesService,
        private val template: AmqpTemplate) {

    fun createNewApartment(form: ApartmentForm, propertyId: Long): Long {
        val property = propertiesService.getProperty(propertyId)
        verifyPropertyOwner(property.managerEmail)
        val meters = form.meters
                .map {
                    MeterEntity(0, it.regNr, it.verificationDate, it.unit,
                            setOf(MeterReading(0, it.reading, Date())))
                }
        val newApartment =
                repository.save(ApartmentEntity(0, meters.toSet(), propertyId, form.number, form.owner.email))
        property.apartments.add(newApartment)
        propertiesService.update(property)

        template.convertAndSend("add_apartment_owner", form.owner)
        return newApartment.id
    }

    fun verifyBillOwner(info: CanUserDownloadBill) {
        val actualOwner = billRepository.findByFileName(info.billName).apartment.ownerEmail
        if (actualOwner != info.userEmail) throw BillNotAccessible
    }

    fun saveBill(bill: BillUploaded) {
        val apartment = repository.findById(bill.apartmentId)
        billRepository.save(Bill(0, bill.fileName, apartment.get(), bill.uploadDate))
    }
}

@Repository
interface ApartmentRepository : CrudRepository<ApartmentEntity, Long>
@Repository
interface BillRepository : CrudRepository<Bill, Long> {
    fun findByFileName(billName: String): Bill
}

@Entity
data class ApartmentEntity(@Id @GeneratedValue val id: Long,
                           @OneToMany(cascade = [CascadeType.PERSIST]) val meters: Set<MeterEntity>,
                           val propertyId: Long,
                           val number: Int,
                           val ownerEmail: String)


@Entity
data class MeterEntity(@Id @GeneratedValue val id: Long,
                       val regNr: String,
                       val verificationDate: Date,
                       val unit: String = "m3",
                       @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST])
                       val readings: Set<MeterReading>)


@Entity
data class MeterReading(@Id @GeneratedValue val id: Long,
                        val value: Float, val date: Date)



@Entity
data class Bill(@Id @GeneratedValue val id: Long, val fileName: String, @OneToOne val apartment: ApartmentEntity, val date: Date)