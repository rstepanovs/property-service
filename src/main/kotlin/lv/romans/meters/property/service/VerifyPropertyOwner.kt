package lv.romans.meters.property.service

import lv.romans.meters.property.controllers.Forbidden
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class VerifyPropertyOwner: (String) -> Unit {
    override fun invoke(propertyManager: String) {
        val requestor = SecurityContextHolder.getContext().authentication.name
        if (requestor != propertyManager) throw Forbidden
    }
}