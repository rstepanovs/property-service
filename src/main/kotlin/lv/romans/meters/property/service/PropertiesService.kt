package lv.romans.meters.property.service

import lv.romans.meters.commons.Address
import lv.romans.meters.commons.api.*
import lv.romans.meters.commons.services.CanUserDownloadBill
import lv.romans.meters.commons.services.Property
import lv.romans.meters.commons.services.PropertyList
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.SQLIntegrityConstraintViolationException
import java.util.*
import javax.persistence.*

@Service
@Transactional
class PropertiesService(private val repository: PropertyRepository,
                        private val verifyPropertyOwner: VerifyPropertyOwner) {

    fun addProperty(property: NewProperty): Long {
        try {
            return repository.save(PropertyEntity(0, PropertyEntity.serializeAddress(property.address), property.managerEmail)).id
        } catch (e: SQLIntegrityConstraintViolationException) {
            throw PropertyIsNotUnique
        }
    }

    fun changeManager(propertyId: Long, newManagerEmail: String) {
        val property = repository.findById(propertyId).get()
        repository.save(property.copy(managerEmail = newManagerEmail))
    }

    fun getAllProperties(): PropertyList {
        return repository.findAll()
                .map { Property(it.id, it.address, it.managerEmail) }
                .run { PropertyList(this) }
    }

    @Transactional
    fun delete(id: Long) {
        repository.deleteById(id)
    }

    fun gePropertiesFor(managerEmail: String) =
            repository.findAllByManagerEmail(managerEmail)
                    .map { PropertySummary(it.id, it.address, it.isSetUp) }


    fun setupProperty(id: Long, form: PropertyForm) {
        val property = repository.findById(id).get()
        verifyPropertyOwner(property.managerEmail)
        val (submit, bill) = form
        submit.toList().plus(bill.toList()).requireNoNulls()
        repository.save(
                property.copy(
                        submitFrom = submit.first, submitTo = submit.second,
                        billingFrom = bill.first, billingTo = bill.second))
    }

    fun setupState(id: Long): PropertySetup {
        val property = repository.findById(id).get()
        verifyPropertyOwner(property.managerEmail)
        return with(property) {
            val apts = apartments.map { PropertySetupApartment(it.id, it.number) }
            println(apts)
            PropertySetup(submitFrom to submitTo,
                    billingFrom to billingTo,
                    apts)
        }
    }

    fun getProperty(propertyId: Long): PropertyEntity {
        val property = repository.findById(propertyId).get()
        verifyPropertyOwner(property.managerEmail)
        return property
    }

    fun update(property: PropertyEntity) {
        repository.save(property)
    }

}


@Repository
interface PropertyRepository : CrudRepository<PropertyEntity, Long> {
    fun findAllByManagerEmail(managerEmail: String): List<PropertyEntity>
}


@Entity
data class PropertyEntity(@Id @GeneratedValue val id: Long,
                          @Column(unique = true) val addressString: String, val managerEmail: String,
                          val billingFrom: Int = 0, val billingTo: Int = 0,
                          val submitFrom: Int = 0, val submitTo: Int = 0,
                          @OneToMany
                          val apartments: MutableSet<ApartmentEntity> = mutableSetOf()) {

    val isSetUp: Boolean
        get() = listOf(billingFrom, billingTo, submitFrom, submitTo).filter { it != 0 }.size == 4

    val address: Address
        get() = addressString.split(";").run { Address(this[0], this[1], this[2].toInt()) }


    companion object {
        fun serializeAddress(address: Address): String {
            val list =
                    address.run { listOf(city, street, streetNum.toString()) }
            if (list.filter { it.contains(';') }.isNotEmpty()) {
                throw IllegalArgumentException("Address cannot contain ; symbol")
            }
            return list.joinToString(";")
        }
    }
}