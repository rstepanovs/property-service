package lv.romans.meters.property.service

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
object PropertyIsNotUnique: Exception()

@ResponseStatus(HttpStatus.FORBIDDEN)
object BillNotAccessible: Exception()